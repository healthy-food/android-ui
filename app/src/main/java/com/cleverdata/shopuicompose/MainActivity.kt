package com.cleverdata.shopuicompose

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.cleverdata.shopuicompose.commons.e.dto.CameraViewAnalysisDto
import com.cleverdata.shopuicompose.commons.e.dto.SettingsDto
import com.cleverdata.shopuicompose.ui.dto.ModeType
import com.cleverdata.shopuicompose.ui.tabContent.tabsWithSwiping
import com.cleverdata.shopuicompose.ui.theme.ShopUIComposeTheme
import com.cleverdata.shopuicompose.utils.CopyDirsUtils
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.MultiplePermissionsState
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import io.reactivex.subjects.BehaviorSubject
import org.bytedeco.javacpp.Loader
import org.bytedeco.opencv.opencv_java
import java.util.*

class MainActivity : ComponentActivity() {

    private lateinit var sharedpreferences: SharedPreferences

    private val APP_SHOP_PRESF = "APP_SHOP_PRESF"
    private var TAG: String = "MAIN_PHOTO"
    private var TAG_START: String = "MAIN_PHOTO_ON_START"

    companion object {
        var pathTess = Environment.getExternalStorageDirectory().toString() + "/OCR"
        val camResText = BehaviorSubject.create<CameraViewAnalysisDto>()
        var analyzeInProcess = false
        val settings = BehaviorSubject.create<SettingsDto>()
        var settingsStored = SettingsDto()
    }



//    override fun attachBaseContext(newBase: Context) {
//        newBase.resources.configuration.setLocale(Locale(MainActivity.settingsStored.language)) // "en"
//
//        applyOverrideConfiguration(newBase.resources.configuration)
//        super.attachBaseContext(newBase)
//    }

//    override fun attachBaseContext(newBase: Context) {
//        /**
//         * handle locale
//         */
//        val currentLang = MainActivity.settingsStored.language//"en" // to get from sharedPref or whatever
//        newBase.resources.configuration.setLocale(Locale(currentLang))
//
//        applyOverrideConfiguration(newBase.resources.configuration)
//        super.attachBaseContext(newBase)
//    }


    @OptIn(ExperimentalPermissionsApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        loadPreferences()
        super.onCreate(savedInstanceState)
        Loader.load(opencv_java::class.java)
        ProcessSubjects()
        setContent {
            ShopUIComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    DefaultPreview()
                }
            }
        }
    }

    private fun loadPreferences() {
        //load setings from persistent storage
        sharedpreferences = getSharedPreferences(APP_SHOP_PRESF, Context.MODE_PRIVATE)
        MainActivity.settingsStored.timestamp =
            sharedpreferences.getBoolean("display_timestamp", false)
        MainActivity.settingsStored.edetails =
            sharedpreferences.getBoolean("display_edetails", true)
        MainActivity.settingsStored.originalText =
            sharedpreferences.getBoolean("display_originaltext", false)
        MainActivity.settingsStored.impovedRecognition =
            sharedpreferences.getBoolean("improved_recognition", true)
        MainActivity.settingsStored.modeType =
            sharedpreferences.getString("mode_type", ModeType.OneEBad.name)
        MainActivity.settingsStored.language =
            sharedpreferences.getString("language", Locale.getDefault().language.toString())


        //settings from Settings tab

    }

    private fun ProcessSubjects() {

        MainActivity.settings.subscribe(
            { newSettings ->
                //get new preferences from user's chpice
                MainActivity.settingsStored = newSettings

                //save new user choice to persistent storage
                val editor = sharedpreferences.edit()

                editor.putBoolean(
                    "display_timestamp",
                    MainActivity.settingsStored.timestamp
                )

                editor.putBoolean(
                    "display_edetails",
                    MainActivity.settingsStored.edetails
                )

                editor.putBoolean(
                    "display_originaltext",
                    MainActivity.settingsStored.originalText
                )

                editor.putBoolean(
                    "improved_recognition",
                    MainActivity.settingsStored.impovedRecognition
                )

                editor.putString(
                    "mode_type",
                    MainActivity.settingsStored.modeType
                )

                editor.commit()

            }
        )


    }


    override fun onStart() {
        super.onStart()
        try {
            if (CopyDirsUtils.copyAssetFolderWasRun && CopyDirsUtils.doExcelParsingWasRun) {
//                Log.d(TAG_START, "Already initialized")
                return;
            } else {
//                Log.d(TAG_START, "Set pathTess")
                pathTess = applicationContext.filesDir.toPath().toString() + "/OCR";
//                Log.d(TAG_START, "Parse excel")
                //process excel files
                CopyDirsUtils.doExcelParsing(applicationContext)
                //copy tessdata
//                Log.d(TAG_START, "Will create tessdata files ")
                CopyDirsUtils.copyAssetFolder(applicationContext, "tessdata", pathTess)
                CopyDirsUtils.copyAssetFolderWasRun = true;
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this@MainActivity, R.string.main_ocr_failed, Toast.LENGTH_LONG).show()
        }

    }

}


@OptIn(ExperimentalPagerApi::class, ExperimentalPermissionsApi::class)
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ShopUIComposeTheme {
        val multiplePermissionsState = rememberMultiplePermissionsState(
            listOf(
                android.Manifest.permission.CAMERA,
            )
        )
        Sample(multiplePermissionsState)
        if (multiplePermissionsState.allPermissionsGranted) {
            tabsWithSwiping()
        }

    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
private fun Sample(multiplePermissionsState: MultiplePermissionsState) {
    if (multiplePermissionsState.allPermissionsGranted) {
        // If all permissions are granted, then show screen with the feature enabled
//        Text("Camera and Read storage permissions Granted! Thank you!")
    } else {
        Column(
            content = {
                Text(
                    getTextToShowGivenPermissions(
                        multiplePermissionsState.revokedPermissions,
                        multiplePermissionsState.shouldShowRationale
                    ),
                    textAlign = TextAlign.Center, style = typography.body1,
                )
                Spacer(modifier = Modifier.height(8.dp))
                Button(onClick = { multiplePermissionsState.launchMultiplePermissionRequest() }) {
                    Text(LocalContext.current.getString(R.string.main_permissions_request_simple))
                }
            }, modifier = Modifier
                .wrapContentSize(Alignment.Center)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        )
    }
}

@Composable
@OptIn(ExperimentalPermissionsApi::class)
private fun getTextToShowGivenPermissions(
    permissions: List<PermissionState>,
    shouldShowRationale: Boolean
): String {
    val revokedPermissionsSize = permissions.size
    if (revokedPermissionsSize == 0) return ""

    val textToShow = StringBuilder().apply {
        append(LocalContext.current.getString(R.string.main_permission_please_request))
    }
    textToShow.append("\n")
    textToShow.append("\n")

    for (i in permissions.indices) {
        textToShow.append(permissions[i].permission)
        textToShow.append("\n")
    }
    textToShow.append("\n")
    textToShow.append(
        if (shouldShowRationale) {
            LocalContext.current.getString(R.string.main_permission_please)
        } else {
            LocalContext.current.getString(R.string.main_permission_warning)
        }
    )
    return textToShow.toString()
}