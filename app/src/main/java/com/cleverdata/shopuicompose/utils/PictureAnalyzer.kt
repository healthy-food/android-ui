package com.cleverdata.shopuicompose.utils

import android.content.Context
import androidx.compose.ui.text.input.TextFieldValue
import androidx.exifinterface.media.ExifInterface
import com.cleverdata.shopuicompose.MainActivity
import com.cleverdata.shopuicompose.commons.e.ProductTextAnalyzerHarm
import com.cleverdata.shopuicompose.commons.e.dto.CameraViewAnalysisDto
import com.cleverdata.shopuicompose.commons.e.dto.HarmAndNamesDto
import org.apache.commons.io.FileUtils
import org.bytedeco.leptonica.global.leptonica
import org.opencv.core.Core
import org.opencv.core.Mat
import org.opencv.imgcodecs.Imgcodecs
import java.io.File


class PictureAnalyzer {

    @Synchronized
    fun analyze(pathStringThresh: String, context: Context): HarmAndNamesDto {


        /**
         * Before analysis, grayscale could be converted to black and white...but intelligently by recognizing mean of grayscale...
         * https://stackoverflow.com/questions/7624765/converting-an-opencv-image-to-black-and-white
         */

        val tempFile = pathStringThresh + "_temp.jpg"
        //copy
        FileUtils.moveFile(File(pathStringThresh), File(tempFile))
        // Create empty Mat object to store output image
        val dst = Mat()
        // Create empty Mat object to store output image
        val src: Mat = Imgcodecs.imread(tempFile)

        val orientation = ImageUtils.getImageRotation(tempFile)

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            Core.rotate(src, dst, Core.ROTATE_90_CLOCKWISE)
            Imgcodecs.imwrite(tempFile, dst)
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            Core.rotate(src, dst, Core.ROTATE_180)
            Imgcodecs.imwrite(tempFile, dst)
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            Core.rotate(src, dst, Core.ROTATE_90_COUNTERCLOCKWISE)
            Imgcodecs.imwrite(tempFile, dst)
        } else {
            // do nothing, image is ok. No rotation
        }


        //clear text
        MainActivity.camResText.onNext(
            CameraViewAnalysisDto(
                TextFieldValue(text = ""),
                TextFieldValue(text = ""),
                TextFieldValue(text = "")
            )
        )


        val tStart = System.currentTimeMillis()

        val api =
            TesseractApiInit.getTessBaseAPIInstance().tessBaseAPI
        val pix = leptonica.pixRead(tempFile)

        //normal
        api.SetImage(pix)
        var outText = api.GetUTF8Text().string
        api.Clear()
        leptonica.pixClearAll(pix)
        leptonica.pixDestroy(pix)

        // inverted
        if (MainActivity.settingsStored.impovedRecognition) {
            val tempFileInverted = pathStringThresh + "_temp_inverted.jpg"
            //copy
            FileUtils.copyFile(File(tempFile), File(tempFileInverted))
            // Create empty Mat object to store output image
            val dstInverted = Mat()
            // Create empty Mat object to store output image
            val srcInverted: Mat = Imgcodecs.imread(tempFileInverted)
            //invert black ,--> white
            Core.bitwise_not(srcInverted, dstInverted)
            Imgcodecs.imwrite(tempFileInverted, dstInverted)

            val pixInverted = leptonica.pixRead(tempFileInverted)
            api.SetImage(pixInverted)
            outText = outText + "\n" + api.GetUTF8Text().string

            api.Clear()
            leptonica.pixClearAll(pixInverted)
            leptonica.pixDestroy(pixInverted)

            val fileThreshInvert = File(tempFileInverted)
            val deletedThreshInvert: Boolean = fileThreshInvert.delete()
        }
        //end inverted


        val tEnd = System.currentTimeMillis()
        val tDelta: Long = tEnd - tStart
        // after done, release the ImageProxy object

        //delete file
        val file = File(pathStringThresh)
        val fileThresh = File(tempFile)

        val deleted: Boolean = file.delete()
        val deletedThresh: Boolean = fileThresh.delete()

        // Get OCR result
        val harm = ProductTextAnalyzerHarm.recognizeEHarmAndNames(outText)
        //change text on screen
        MainActivity.camResText.onNext(
            CameraTextProducer.produceText(
                harm,
                MainActivity.settingsStored.timestamp,
                MainActivity.settingsStored.edetails,
                MainActivity.settingsStored.originalText,
                tDelta,
                context
            )

        )
        return harm
    }

}
