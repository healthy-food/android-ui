package com.cleverdata.shopuicompose.utils;

import com.cleverdata.shopuicompose.MainActivity;

import org.bytedeco.tesseract.TessBaseAPI;
import org.bytedeco.tesseract.global.tesseract;


/**
 * Class providing tesseract initialization
 */
public class TesseractApiInit {

    public TessBaseAPI getTessBaseAPI() {
        return tessBaseAPI;
    }

    private TesseractApiInit() {
        tessBaseAPI = new TessBaseAPI();
        tessBaseAPI.SetPageSegMode(tesseract.PSM_AUTO_OSD);
        //"ces+slk+pol+deu+eng+lat+lit+srp_latn"
        if (tessBaseAPI.Init(MainActivity.Companion.getPathTess() + "/fast", "ces+slk", tesseract.OEM_DEFAULT) != 0) {
            throw new RuntimeException("Could not initialize fast tesseract.");
        }

        tessBaseAPI.SetPageSegMode(tesseract.PSM_AUTO_OSD);
//        tessBaseAPI.SetVariable("tessedit_do_invert","0"); //this works only for first language https://github.com/tesseract-ocr/tesseract/issues/3037
    }

    private static TesseractApiInit instance = null;

    private TessBaseAPI tessBaseAPI;

    public static TesseractApiInit getTessBaseAPIInstance() {
        if (instance != null) {
            return instance;
        } else {
            instance = new TesseractApiInit();
            return instance;
        }
    }

    public static void destroyCurrentInstance() throws Throwable {
        if (instance != null) {
            instance.finalize();
            instance = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        //end programatically
        tessBaseAPI.Clear();
        tessBaseAPI.End();
        super.finalize();
    }

}
