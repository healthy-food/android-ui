package com.cleverdata.shopuicompose.utils

import android.content.Context
import android.content.res.Resources
import androidx.compose.ui.text.input.TextFieldValue
import com.cleverdata.shopuicompose.R
import com.cleverdata.shopuicompose.commons.e.dto.CameraViewAnalysisDto
import com.cleverdata.shopuicompose.commons.e.dto.HarmAndNamesDto
import java.text.SimpleDateFormat
import java.util.*

/**
 * Produces visual text from analyse output
 */
object CameraTextProducer {
    fun produceText(harmAndNamesDto: HarmAndNamesDto, addTimestamp: Boolean, addDetails: Boolean, originalText: Boolean, miliseconds: Long, context: Context): CameraViewAnalysisDto {
        val found = harmAndNamesDto.getNames()
        val sbEanalysis = StringBuffer()
        sbEanalysis.append(context.getString(R.string.analyseoutput_totalharm) + harmAndNamesDto.getHarm())
        sbEanalysis.append(System.getProperty("line.separator"))
        sbEanalysis.append(System.getProperty("line.separator"))
        if (addDetails && found.isNotEmpty()) {
            for (eDto in found) {
                sbEanalysis.append(eDto.getSource() + " " + eDto.code + "   " + eDto.harmLevel + "   " + eDto.name)
                sbEanalysis.append(System.getProperty("line.separator"))
            }
        }
        val sbTimestamp = StringBuffer()
        if (addTimestamp) {
            val local = Resources.getSystem().configuration.locales[0]
            val sdf = SimpleDateFormat("yyyy-MM-dd.hh.mm.ss")
//            val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, local)
            val stringDate = sdf.format(Date()).uppercase(local)
            val seconds = miliseconds /100 /10.0
            sbTimestamp.append("$stringDate -> $seconds s")
        }
        val sbOriginalText = StringBuffer()
        if (originalText) {
            sbOriginalText.append(harmAndNamesDto.getOriginalText().replace("\\s+".toRegex(), " "))
            sbOriginalText.append(System.getProperty("line.separator"))
        }
        //        return sb.toString();
        return CameraViewAnalysisDto(TextFieldValue(text = sbTimestamp.toString()),TextFieldValue(text = sbOriginalText.toString()),TextFieldValue(text = sbEanalysis.toString()))
    }
}