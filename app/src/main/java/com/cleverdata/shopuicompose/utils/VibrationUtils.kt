package com.cleverdata.shopuicompose.utils;

import android.content.Context
import android.os.Vibrator
import com.cleverdata.shopuicompose.MainActivity
import com.cleverdata.shopuicompose.commons.e.dto.HarmAndNamesDto
import com.cleverdata.shopuicompose.ui.dto.ModeType

public class VibrationUtils {

    fun vibrateOnEnd(context: Context){
        val modetypeName = MainActivity.settingsStored.modeType

        when (modetypeName) {
            ModeType.SilentVibrateMode.name -> {
                //no vibration
            }
            else -> {
                val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator;
//                val pattern = longArrayOf(0, 150,50,150,50,150)
                val pattern = longArrayOf(0, 500)
                vibrator.vibrate(pattern, -1)
            }
        }

    }

    fun vibrateOnButtonClick(context: Context){
        val modetypeName = MainActivity.settingsStored.modeType

        when (modetypeName) {
            ModeType.SilentVibrateMode.name -> {
                //no vibration
            }
            else -> {
                val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator;
                val pattern = longArrayOf(0, 400)
                vibrator.vibrate(pattern, -1)
            }
        }


    }

    fun vibrateAfterAnalyzeToGiveFeedback(
        context: Context,
        harmAndNamesDto: HarmAndNamesDto
    ) {
        val vibrator =
            context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator;

        val modetypeName = MainActivity.settingsStored.modeType


        // Start without a delay
        // Each element then alternates between vibrate, sleep, vibrate, sleep...
//        val pattern = longArrayOf(0, 200, 100, 200, 100, 200, 100, 700)
        val pattern = longArrayOf(0, 2000)


        when (modetypeName) {
            ModeType.OneEBad.name -> {
                //create rule
                if (harmAndNamesDto.names.stream().filter { it.harmLevel > 3 }.count() > 0) {
                    vibrator.vibrate(pattern, -1);
                }
            }
            ModeType.GenerallyBad.name -> {
                //create rule
                if (harmAndNamesDto.names.stream().filter { it.harmLevel < 2 }.count() > 3) {
                    vibrator.vibrate(pattern, -1);
                }

            }
            ModeType.SilentVibrateMode.name -> {
                //no vibration
            }
            else -> {
                //shouldn't really happened
            }
        }


    }

}
