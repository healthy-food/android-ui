package com.cleverdata.shopuicompose.utils;

import android.content.Context;

import com.cleverdata.shopuicompose.commons.e.ELoader;
import com.cleverdata.shopuicompose.commons.e.dto.EDto;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;


/**
 * Utils for handling resources
 */
public class CopyDirsUtils {

    public static boolean copyAssetFolderWasRun = false, doExcelParsingWasRun = false;

//
//    public static void deleteRecursive(File fileOrDirectory, String suffix,boolean recursive) {
//        if (deleteRecursiveWasRun && !recursive) {
//            return;
//        }
//        deleteRecursiveWasRun = true;
//        if (fileOrDirectory.isDirectory())
//            for (File child : fileOrDirectory.listFiles())
//                deleteRecursive(child, suffix,true);
//
//        if (fileOrDirectory.getName().endsWith(suffix)) {
//            fileOrDirectory.delete();
//        }
//    }


    public static void copyAssetFolder(Context context, String srcName, String dstName) {
        if (copyAssetFolderWasRun) {
            return;
        }
        try {
            String fileList[] = context.getAssets().list(srcName);
            if (fileList == null) return;

            if (fileList.length == 0) {
                copyAssetFile(context, srcName, dstName);
            } else {
                File file = new File(dstName);
                file.mkdirs();
                for (String filename : fileList) {
                    copyAssetFolder(context, srcName + File.separator + filename, dstName + File.separator + filename);
                }
            }
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void copyAssetFile(Context context, String srcName, String dstName) {
        try {
            if (!(new File(dstName)).exists()) {
                InputStream in = context.getAssets().open(srcName);
                File outFile = new File(dstName);
                OutputStream out = new FileOutputStream(outFile);
                byte[] buffer = new byte[1024];
                int read;
                while ((read = in.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }
                in.close();
                out.close();
            }
//            Log.d("CopyDirsUtils file : ", dstName);
            return;
        } catch (IOException e) {
//            e.printStackTrace();
            return;
        }
    }

    public static void doExcelParsing(Context context) throws IOException, CsvValidationException {
        if (doExcelParsingWasRun) {
            return;
        }
        doExcelParsingWasRun = true;
        InputStreamReader eFileInputStreamReader = null;
        /**
         * Android
         */
        for (String efile : context.getAssets().list("enumbers")) {
            try {
                eFileInputStreamReader = new InputStreamReader(context.getAssets().open("enumbers/" + efile));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }


            // create csvParser object with
            // custom seperator semi-colon
            CSVParser parser = new CSVParserBuilder().withSeparator(',').build();

            // create csvReader object with parameter
            // filereader and parser
            CSVReader csvReader = new CSVReaderBuilder(eFileInputStreamReader)
                    .withCSVParser(parser).withSkipLines(1)
                    .build();
            String[] nextRecord;

            int index = 0;
            int ecko = 1;
            int jmeno = 2;
            int harmLevel = 3;
            // we are going to read data line by line
            while (true) {
                if ((nextRecord = csvReader.readNext()) == null) break;

                //nextrecort shall be not empty
                if (StringUtils.isNotBlank(nextRecord[index])) {
                    if (nextRecord[jmeno].contains(";")) {
                        for (String newName : nextRecord[jmeno].split(";")) {
                            ELoader.eMap.put(nextRecord[index], EDto.builder().index(nextRecord[index]).code(nextRecord[ecko].toUpperCase()).name(newName.toUpperCase()).harmLevel(parseInt(nextRecord[harmLevel])).build());

                        }
                    } else {
                        ELoader.eMap.put(nextRecord[index], EDto.builder().index(nextRecord[index]).code(nextRecord[ecko].toUpperCase()).name(nextRecord[jmeno].toUpperCase()).harmLevel(parseInt(nextRecord[harmLevel])).build());
                    }
                }
            }
        }
        return;
    }


    private static Integer parseInt(String toParse) {
        if (StringUtils.isBlank(toParse.trim())) {
            return 0;
        }
        if (!NumberUtils.isDigits(toParse)) {
            return 0;
        }
        return Integer.parseInt(toParse);
    }

}