package com.cleverdata.shopuicompose.commons.e.eanalysis;

import com.cleverdata.shopuicompose.commons.e.dto.ExtendedDto;

import java.util.Set;

/**
 * Generic interface wrapping analyse
 */
public interface AnalyticsInterface {

    /**
     * Implement exact analitics method here
     * @param recognizedText
     * @return
     */
    public Set<ExtendedDto> analyzeText(String recognizedText);

}
