package com.cleverdata.shopuicompose.commons.e.dto;

/**
 * Represents one addtitivum. Can be written in many ways on label.
 */
public class EDto {

    /**
     * This is duplicate of index in map
     */
    protected String index;

    /**
     * Nme of E like  E250
     */
    private String code;

    /**
     * Name of E. More E can exists for on code.
     */
    //TODO:Refactor to list or someting like this
    private String name;

    /**
     * Is from animals
     */
    private Boolean isAnimalOrigin;

    /**
     * Harm level for human body
     * Usually:
     * -1...no harm, but good
     * 0...no negative effect
     * 1...light negative effect
     * 2...mild negative effect
     * 3...negative effect, food shouldn't be eaten from this level
     * 4...very bad
     * 5...extremely bad, most of E in this group is forbidden to use(not all)
     */
    private Integer harmLevel;


    EDto(String index, String code, String name, Boolean isAnimalOrigin, Integer harmLevel) {
        this.index = index;
        this.code = code;
        this.name = name;
        this.isAnimalOrigin = isAnimalOrigin;
        this.harmLevel = harmLevel;
    }

    public EDto() {
    }

    public static EDtoBuilder builder() {
        return new EDtoBuilder();
    }

    public String getIndex() {
        return this.index;
    }

    public String getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

    public Boolean getIsAnimalOrigin() {
        return this.isAnimalOrigin;
    }

    public Integer getHarmLevel() {
        return this.harmLevel;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIsAnimalOrigin(Boolean isAnimalOrigin) {
        this.isAnimalOrigin = isAnimalOrigin;
    }

    public void setHarmLevel(Integer harmLevel) {
        this.harmLevel = harmLevel;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof EDto)) return false;
        final EDto other = (EDto) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$index = this.getIndex();
        final Object other$index = other.getIndex();
        if (this$index == null ? other$index != null : !this$index.equals(other$index)) return false;
        final Object this$code = this.getCode();
        final Object other$code = other.getCode();
        if (this$code == null ? other$code != null : !this$code.equals(other$code)) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$isAnimalOrigin = this.getIsAnimalOrigin();
        final Object other$isAnimalOrigin = other.getIsAnimalOrigin();
        if (this$isAnimalOrigin == null ? other$isAnimalOrigin != null : !this$isAnimalOrigin.equals(other$isAnimalOrigin))
            return false;
        final Object this$harmLevel = this.getHarmLevel();
        final Object other$harmLevel = other.getHarmLevel();
        if (this$harmLevel == null ? other$harmLevel != null : !this$harmLevel.equals(other$harmLevel)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof EDto;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $index = this.getIndex();
        result = result * PRIME + ($index == null ? 43 : $index.hashCode());
        final Object $code = this.getCode();
        result = result * PRIME + ($code == null ? 43 : $code.hashCode());
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        final Object $isAnimalOrigin = this.getIsAnimalOrigin();
        result = result * PRIME + ($isAnimalOrigin == null ? 43 : $isAnimalOrigin.hashCode());
        final Object $harmLevel = this.getHarmLevel();
        result = result * PRIME + ($harmLevel == null ? 43 : $harmLevel.hashCode());
        return result;
    }

    public String toString() {
        return "EDto(index=" + this.getIndex() + ", code=" + this.getCode() + ", name=" + this.getName() + ", isAnimalOrigin=" + this.getIsAnimalOrigin() + ", harmLevel=" + this.getHarmLevel() + ")";
    }

    public static class EDtoBuilder {
        private String index;
        private String code;
        private String name;
        private Boolean isAnimalOrigin;
        private Integer harmLevel;

        EDtoBuilder() {
        }

        public EDtoBuilder index(String index) {
            this.index = index;
            return this;
        }

        public EDtoBuilder code(String code) {
            this.code = code;
            return this;
        }

        public EDtoBuilder name(String name) {
            this.name = name;
            return this;
        }

        public EDtoBuilder isAnimalOrigin(Boolean isAnimalOrigin) {
            this.isAnimalOrigin = isAnimalOrigin;
            return this;
        }

        public EDtoBuilder harmLevel(Integer harmLevel) {
            this.harmLevel = harmLevel;
            return this;
        }

        public EDto build() {
            return new EDto(index, code, name, isAnimalOrigin, harmLevel);
        }

        public String toString() {
            return "EDto.EDtoBuilder(index=" + this.index + ", code=" + this.code + ", name=" + this.name + ", isAnimalOrigin=" + this.isAnimalOrigin + ", harmLevel=" + this.harmLevel + ")";
        }
    }
}
