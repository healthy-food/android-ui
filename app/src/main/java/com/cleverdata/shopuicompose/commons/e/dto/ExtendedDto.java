package com.cleverdata.shopuicompose.commons.e.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class ExtendedDto extends EDto{

    /**
     * Source is Ecode or Name, depends where program red it from
     */
    public String source;

    public ExtendedDto(String index, String code, String name, Boolean isAnimalOrigin, Integer harmLevel, String source) {
        super(index, code, name, isAnimalOrigin, harmLevel);
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ExtendedDto that = (ExtendedDto) o;
        // equality should depend only on indexes
        return new EqualsBuilder().append(index, that.index).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(index).toHashCode();
    }
}
