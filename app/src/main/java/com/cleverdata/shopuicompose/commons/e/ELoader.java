package com.cleverdata.shopuicompose.commons.e;

import com.cleverdata.shopuicompose.commons.e.dto.EDto;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * E loader for E rom excel.It is writen as it is becasu would like to use it in android without rewriting it
 */
public class ELoader {


    public static Map<String, EDto> eMap = new HashMap<>();



    /**
     * Return ecodes.
     * TODO: To stream if android supports it
     *
     * @return
     */
    public static List<String> getECodesLowercase() {
        List<String> eCodes = new ArrayList<>();
        Collection<EDto> eDtos = eMap.values();
        for (EDto dto : eDtos) {
            eCodes.add(dto.getCode());
        }
        return eCodes;
    }

    /**
     * Return eNames.
     * TODO: To stream if android supports it
     *
     * @return
     */
    public static List<String> getENamesLowercase() {
        List<String> eNames = new ArrayList<>();
        Collection<EDto> eDtos = eMap.values();
        for (EDto dto : eDtos) {
            eNames.add(dto.getName());
        }
        return eNames;
    }



    /**
     * Now only for one language
     *
     * @param e
     * @return
     */
    public static Integer harmOfEcodeOrEnamePresent(String e) {
        if (StringUtils.isBlank(e))
            return null;
        e = e.trim();

        for (EDto eDto : eMap.values()) {
            if (eDto.getCode().equalsIgnoreCase(e) || eDto.getName().equalsIgnoreCase(e)) {
                return eDto.getHarmLevel();
            }
        }

        /**
         * Multilanguage
         */
//        for(String key : eMap.keySet()){
//            if(eMap.get(k)) //find the same index all over supported languages. TODO: Supported languages means also tesseract recognizes them in text
//        }
        //nothing found, return no harming E
        return 0;
    }


}
