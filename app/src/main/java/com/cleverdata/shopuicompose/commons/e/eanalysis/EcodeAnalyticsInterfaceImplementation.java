package com.cleverdata.shopuicompose.commons.e.eanalysis;

import com.cleverdata.shopuicompose.commons.e.ELoader;
import com.cleverdata.shopuicompose.commons.e.dto.EDto;
import com.cleverdata.shopuicompose.commons.e.dto.ExtendedDto;
import com.cleverdata.shopuicompose.commons.e.dto.SourceEnum;

import java.util.HashSet;
import java.util.Set;

public class EcodeAnalyticsInterfaceImplementation implements AnalyticsInterface {

    @Override
    public Set<ExtendedDto> analyzeText(String recognizedText){

        /*
         * text to one string "E 250" to "E250"
         */
        String wholeStringForEanalysis = recognizedText.replaceAll("\\s+", "");

        //instantiate dto
        Set<ExtendedDto> names = new HashSet<>();
        //Exxx
        for (EDto code : ELoader.eMap.values()) {
            if (wholeStringForEanalysis.contains(code.getCode())) {
                //E codes musts fit, no change there
                names.add(new ExtendedDto(code.getIndex(), code.getCode(), code.getName(), code.getIsAnimalOrigin(), code.getHarmLevel(), SourceEnum.Ecode));
            }
        }

        return names;
    }
}
