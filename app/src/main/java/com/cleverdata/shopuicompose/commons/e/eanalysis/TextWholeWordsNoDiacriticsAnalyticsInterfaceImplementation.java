package com.cleverdata.shopuicompose.commons.e.eanalysis;

import com.cleverdata.shopuicompose.commons.e.ELoader;
import com.cleverdata.shopuicompose.commons.e.dto.EDto;
import com.cleverdata.shopuicompose.commons.e.dto.ExtendedDto;
import com.cleverdata.shopuicompose.commons.e.dto.SourceEnum;

import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Search for whole words without diactitics
 */
public class TextWholeWordsNoDiacriticsAnalyticsInterfaceImplementation implements AnalyticsInterface {
    @Override
    public Set<ExtendedDto> analyzeText(String recognizedText) {

        //prepare text

        //instantiate dto
        Set<ExtendedDto> names = new HashSet<>();

        for (EDto name : ELoader.eMap.values()) {
            if (StringUtils.stripAccents(recognizedText).contains(StringUtils.stripAccents(name.getName()))) {
                names.add(new ExtendedDto(name.getIndex(), name.getCode(), name.getName(), name.getIsAnimalOrigin(), name.getHarmLevel(), SourceEnum.TextNoDiacritics));

            }
        }
        return names;
    }
}