package com.cleverdata.shopuicompose.commons.e;

import com.cleverdata.shopuicompose.commons.e.dto.EDto;
import com.cleverdata.shopuicompose.commons.e.dto.ExtendedDto;
import com.cleverdata.shopuicompose.commons.e.dto.HarmAndNamesDto;
import com.cleverdata.shopuicompose.commons.e.dto.SourceEnum;
import com.cleverdata.shopuicompose.commons.e.eanalysis.EcodeAnalyticsInterfaceImplementation;
import com.cleverdata.shopuicompose.commons.e.eanalysis.TextWholeWordsDiacriticsAnalyticsInterfaceImplementation;
import com.cleverdata.shopuicompose.commons.e.eanalysis.TextWholeWordsNoDiacriticsAnalyticsInterfaceImplementation;
import com.cleverdata.shopuicompose.commons.e.eanalysis.WordsLevenshteinDistanceNoDiacriticsAnalyticsInterfaceImplementation;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Count harmness of each E together or other types of harmness
 */
public class ProductTextAnalyzerHarm {


    /**
     * Produces analysis of original recognized text
     *
     * @param recognizedText
     * @return
     */
    public static HarmAndNamesDto recognizeEHarmAndNames(String recognizedText) {
        HarmAndNamesDto harmAndNamesDto = new HarmAndNamesDto();
        /**
         * keeps the original text before analysis to be displayed on phone screen
         */
        harmAndNamesDto.originalText = recognizedText;
        /**
         * all recognized text to upper case, divide words by space
         */
        recognizedText = recognizedText.replaceAll("\\s+", " ").toUpperCase();
        /*
         * all spaces and strange text transformed to one space. This should help recognize words
         */
        String[] recognizedWordsForTextAnalysis = recognizedText.split(" ");
        /*
         * text to one string "E 250" to "E250"
         */
        String wholeStringForEanalysis = recognizedText.replaceAll("\\s+", "");


        Integer overallHarm = 0;


        /**
         * All analyses here
         */
        Set<ExtendedDto> ecodeSet= new EcodeAnalyticsInterfaceImplementation().analyzeText(recognizedText);
//        Set<ExtendedDto> textDiactiticsSet= new TextWholeWordsDiacriticsAnalyticsInterfaceImplementation().analyzeText(recognizedText);
//        Set<ExtendedDto> textNoDiactiticsSet= new TextWholeWordsNoDiacriticsAnalyticsInterfaceImplementation().analyzeText(recognizedText);
        Set<ExtendedDto> wordLevenshteinDistanceDiactiticsSet= new WordsLevenshteinDistanceNoDiacriticsAnalyticsInterfaceImplementation().analyzeText(recognizedText);

        /**
         * Collect analyses
         */
        harmAndNamesDto.names.addAll(ecodeSet);
//        harmAndNamesDto.names.addAll(textDiactiticsSet);
//        harmAndNamesDto.names.addAll(textNoDiactiticsSet);
        harmAndNamesDto.names.addAll(wordLevenshteinDistanceDiactiticsSet);


        //Sum of all harm here
        for(ExtendedDto extendedDto: harmAndNamesDto.names){
            overallHarm+=extendedDto.getHarmLevel();
        }

        harmAndNamesDto.setHarm(overallHarm);
        return harmAndNamesDto;

    }


}
