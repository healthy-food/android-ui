package com.cleverdata.shopuicompose.commons.e.dto;

import java.util.HashSet;
import java.util.Set;

public class HarmAndNamesDto {
    public String originalText;
    public int harm;
    public Set<ExtendedDto> names = new HashSet<>();

    public int getHarm() {
        return harm;
    }

    public void setHarm(int harm) {
        this.harm = harm;
    }

    public Set<ExtendedDto> getNames() {
        return names;
    }

    public String getOriginalText() {
        return originalText;
    }

}
