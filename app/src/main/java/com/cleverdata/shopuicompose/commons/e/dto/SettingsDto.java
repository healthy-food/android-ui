package com.cleverdata.shopuicompose.commons.e.dto;

public class SettingsDto {

    public SettingsDto(){

    }

    public SettingsDto(Boolean timestamp, Boolean edetails, Boolean originalText,String modeType, String language, Boolean impovedRecognition) {
        this.timestamp = timestamp;
        this.edetails = edetails;
        this.originalText = originalText;
        this.modeType = modeType;
        this.language = language;
        this.impovedRecognition = impovedRecognition;
    }

    public Boolean timestamp;
    public Boolean edetails;
    public Boolean originalText;
    public String modeType;
    public String language;
    public Boolean impovedRecognition;


}
