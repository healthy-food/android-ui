package com.cleverdata.shopuicompose.commons.e.eanalysis;

import com.cleverdata.shopuicompose.commons.e.ELoader;
import com.cleverdata.shopuicompose.commons.e.dto.EDto;
import com.cleverdata.shopuicompose.commons.e.dto.ExtendedDto;
import com.cleverdata.shopuicompose.commons.e.dto.SourceEnum;

import java.util.HashSet;
import java.util.Set;

/**
 * Search for text matches with diacritics
 */
public class TextWholeWordsDiacriticsAnalyticsInterfaceImplementation implements AnalyticsInterface {
    @Override
    public Set<ExtendedDto> analyzeText(String recognizedText) {
        //prepare text

        //instantiate dto
        Set<ExtendedDto> names = new HashSet<>();

        for (EDto name : ELoader.eMap.values()) {
            if (recognizedText.contains(name.getName())) {
                names.add(new ExtendedDto(name.getIndex(), name.getCode(), name.getName(), name.getIsAnimalOrigin(), name.getHarmLevel(), SourceEnum.Text));
            }
        }
        return names;
    }
}
