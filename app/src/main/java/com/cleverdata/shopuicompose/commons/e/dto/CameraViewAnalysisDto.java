package com.cleverdata.shopuicompose.commons.e.dto;

import androidx.compose.ui.text.input.TextFieldValue;

public class CameraViewAnalysisDto {
    public TextFieldValue timestamp;
    public TextFieldValue originalText;
    public TextFieldValue eAnalysis;

    public Boolean empty() {
        return timestamp.getText().isEmpty() && originalText.getText().isEmpty() && eAnalysis.getText().isEmpty();
    }

    public CameraViewAnalysisDto(TextFieldValue timestamp, TextFieldValue originalText, TextFieldValue eAnalysis) {
        this.timestamp = timestamp;
        this.originalText = originalText;
        this.eAnalysis = eAnalysis;
    }

}
