package com.cleverdata.shopuicompose.commons.e.dto;

/**
 * Ecode=(E), Text recognized = (T)
 */
public class SourceEnum {
    public static String Ecode = "(E)";
    public static String Text = "(T)";
    public static String TextApproximation = "(TA)";
    public static String TextNoDiacritics = "(TX)";
    public static String TextApproximationNoDiacritics = "(TAX)";
}
