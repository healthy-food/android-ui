package com.cleverdata.shopuicompose.commons.e.eanalysis;

import com.cleverdata.shopuicompose.commons.e.ELoader;
import com.cleverdata.shopuicompose.commons.e.dto.EDto;
import com.cleverdata.shopuicompose.commons.e.dto.ExtendedDto;
import com.cleverdata.shopuicompose.commons.e.dto.SourceEnum;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Search for whole words without diactitics
 */
public class WordsLevenshteinDistanceNoDiacriticsAnalyticsInterfaceImplementation implements AnalyticsInterface {
    @Override
    public Set<ExtendedDto> analyzeText(String recognizedText) {

        //instantiate dto
        Set<ExtendedDto> names = new HashSet<>();

        if (recognizedText == null || recognizedText.isBlank()) {
            return names;
        }

        //get all words from names
//        Set<String> words = ELoader.eMap.values().stream().map(EDto::getName).collect(Collectors.toSet());
//
//        //recognize shortesr word in names
//        String longestWord = words.stream().max(Comparator.comparing(String::length)).get();
//        int longestWordSize = longestWord.length();
//        //recognize longest word in names
//        String shortestWord = words.stream().min(Comparator.comparing(String::length)).get();
//        int shortestWordSize = shortestWord.length();

//        if(shortestWordSize < 6){
//            shortestWordSize = 6; //reasonable limit
//        }
//
//        if(longestWordSize > 15){
//            longestWordSize = 15; //reasonable limit
//        }

//        Set<String> uniqueWords = new HashSet<>();

        //prepare text...comment for now, too many choices result in slow recognition
//        for (int i = 0; i < recognizedText.length(); i++) {
//            for (int wordSize = shortestWordSize; wordSize <= longestWordSize; wordSize++) {
//                if((i+wordSize) < recognizedText.length()) {
//                    uniqueWords.add(StringUtils.stripAccents(recognizedText.substring(i, i + wordSize)));
//                }
//            }
//        }

//        uniqueWords.addAll(Arrays.stream(recognizedText.replace("("," ").replace(")"," ").split(" ")).collect(Collectors.toList()));

        //put words together

        LevenshteinDistance calculator = new LevenshteinDistance();
        //length of recognized text
        int lenText =recognizedText.length();
        for (EDto name : ELoader.eMap.values()) {
            // length of word to be found
            int lenWord = name.getName().length();
            //if word is found in text, then text minus word should be the distance
            //2 is levenshtein distance
            if (calculator.apply(StringUtils.stripAccents(recognizedText), StringUtils.stripAccents(name.getName())) < (lenText - lenWord + 2)) { //threshold will be always 1 or less, as given by constructor
                names.add(new ExtendedDto(name.getIndex(), name.getCode(), name.getName(), name.getIsAnimalOrigin(), name.getHarmLevel(), SourceEnum.TextApproximationNoDiacritics));
            }
        }

        return names;
    }
}