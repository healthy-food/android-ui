package com.cleverdata.shopuicompose.ui.tabContent


import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.cleverdata.shopuicompose.MainActivity
import com.cleverdata.shopuicompose.R
import com.cleverdata.shopuicompose.ui.settingsParts.CameraViewCheckboxSettingsComponent

@Preview
@Composable
fun SettingsScreenForTab() {
    Column(
        content = {
            Row {
                CameraViewCheckboxSettingsComponent()
            }
            Row {
                Button(onClick = {
                    MainActivity.settings.onComplete()
                    MainActivity.camResText.onComplete()
                    MainActivity().finishAndRemoveTask()
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                }, enabled = true, modifier = Modifier.fillMaxWidth(0.9f)) {
                    Text(text = LocalContext.current.getString(R.string.tabcontent_settings_tab_quit))
                }
            }
        }, modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    )
}
