package com.cleverdata.shopuicompose.ui.tabContent

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.cleverdata.shopuicompose.ui.modesParts.VibrationSettingsComponent


@Preview
@Composable
fun ModesScreenForTab() {
    Box {
        Column(
            content = {
                Column {
                    VibrationSettingsComponent()
//                    RecognitionQualitySettingsComponent()
                }
            }, modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        )
    }
}
