package com.cleverdata.shopuicompose.ui.settingsParts


import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.cleverdata.shopuicompose.MainActivity
import com.cleverdata.shopuicompose.R
import com.cleverdata.shopuicompose.commons.e.dto.SettingsDto


@Composable
fun CameraViewCheckboxSettingsComponent() {
    Row {
        Column()
        {

            val timestamp = remember { mutableStateOf(MainActivity.settingsStored.timestamp) }
            val eDetails = remember { mutableStateOf(MainActivity.settingsStored.edetails) }
            val improvedRecognition = remember { mutableStateOf(MainActivity.settingsStored.impovedRecognition) }
            val originalText =
                remember { mutableStateOf(MainActivity.settingsStored.originalText) }

            Text(
                text = LocalContext.current.getString(R.string.settings_details_on_camera_screen),
                modifier = Modifier.padding(16.dp)
            )


            Row {
                Checkbox(
                    checked = eDetails.value,
                    modifier = Modifier.padding(16.dp),
                    colors = CheckboxDefaults.colors(checkedColor = Color.Blue),
                    onCheckedChange = {
                        eDetails.value = it
                        MainActivity.settings.onNext(
                            SettingsDto(
                                timestamp.value,
                                eDetails.value,
                                originalText.value,
                                MainActivity.settingsStored.modeType,
                                MainActivity.settingsStored.language,
                                improvedRecognition.value
                            )
                        )
                    },
                )
                Text(
                    text = LocalContext.current.getString(R.string.settings_recognized_additives),
                    modifier = Modifier.padding(26.dp)
                )
            }
            Row {
                Checkbox(
                    checked = originalText.value,
                    modifier = Modifier.padding(16.dp),
                    colors = CheckboxDefaults.colors(checkedColor = Color.Blue),
                    onCheckedChange = {
                        originalText.value = it
                        MainActivity.settings.onNext(
                            SettingsDto(
                                timestamp.value,
                                eDetails.value,
                                originalText.value,
                                MainActivity.settingsStored.modeType,
                                MainActivity.settingsStored.language,
                                improvedRecognition.value
                            )
                        )
                    },
                )
                Text(
                    text = LocalContext.current.getString(R.string.settings_original_text),
                    modifier = Modifier.padding(26.dp)
                )
            }
            Row {
                Checkbox(
                    checked = timestamp.value,
                    modifier = Modifier.padding(16.dp),
                    colors = CheckboxDefaults.colors(checkedColor = Color.Blue),
                    onCheckedChange = {
                        timestamp.value = it
                        MainActivity.settings.onNext(
                            SettingsDto(
                                timestamp.value,
                                eDetails.value,
                                originalText.value,
                                MainActivity.settingsStored.modeType,
                                MainActivity.settingsStored.language,
                                improvedRecognition.value
                            )
                        )
                    },
                )
//        Spacer(modifier = Modifier.size(16.dp))
                Text(
                    text = LocalContext.current.getString(R.string.settings_current_timestamp),
                    modifier = Modifier.padding(26.dp)
                )
            }

            Text(
                text = LocalContext.current.getString(R.string.settings_recognition_details),
                modifier = Modifier.padding(16.dp)
            )


            Row {
                Checkbox(
                    checked = improvedRecognition.value,
                    modifier = Modifier.padding(16.dp),
                    colors = CheckboxDefaults.colors(checkedColor = Color.Blue),
                    onCheckedChange = {
                        improvedRecognition.value = it
                        MainActivity.settings.onNext(
                            SettingsDto(
                                timestamp.value,
                                eDetails.value,
                                originalText.value,
                                MainActivity.settingsStored.modeType,
                                MainActivity.settingsStored.language,
                                improvedRecognition.value
                            )
                        )
                    },
                )
                Text(
                    text = LocalContext.current.getString(R.string.settings_recognition_improved),
                    modifier = Modifier.padding(26.dp)
                )
            }

        }
    }
}
