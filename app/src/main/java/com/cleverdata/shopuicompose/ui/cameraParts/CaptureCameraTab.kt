package com.cleverdata.shopuicompose.ui.cameraParts

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.hardware.camera2.CameraMetadata.CONTROL_EFFECT_MODE_MONO
import android.hardware.camera2.CaptureRequest
import android.util.Rational
import android.view.ViewGroup
import androidx.camera.camera2.interop.Camera2Interop
import androidx.camera.core.*
import androidx.camera.core.ImageCapture.FLASH_MODE_AUTO
import androidx.camera.core.ImageCapture.FLASH_TYPE_USE_TORCH_AS_FLASH
import androidx.camera.extensions.ExtensionMode
import androidx.camera.extensions.ExtensionsManager
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.window.layout.WindowMetrics
import androidx.window.layout.WindowMetricsCalculator
import com.cleverdata.shopuicompose.MainActivity
import com.cleverdata.shopuicompose.R
import com.cleverdata.shopuicompose.commons.e.dto.CameraViewAnalysisDto
import com.cleverdata.shopuicompose.utils.PictureAnalyzer
import com.cleverdata.shopuicompose.utils.VibrationUtils
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.util.concurrent.Executor
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


@SuppressLint("UnsafeOptInUsageError", "UnrememberedMutableState", "RestrictedApi")
@ExperimentalPagerApi // 1.
@Composable
fun CaptureCameraTab(
    modifier: Modifier = Modifier.fillMaxSize(),
    cameraSelector: CameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
) {

    val context = LocalContext.current
    var previewView by remember {
        mutableStateOf(PreviewView(context).apply {
            this.scaleType = scaleType
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        })
    }
    var imageCaptureFinal by remember { mutableStateOf(ImageCapture.Builder().build()) }
    //text with analysis is present
    var isAllViewVisible by remember { mutableStateOf(false) }
    //original text
    val textOriginal = remember { mutableStateOf(TextFieldValue()) }
    //eAnalysis
    val textEAnalysis = remember { mutableStateOf(TextFieldValue()) }
    //text initial setting
    val textTimestamp = remember { mutableStateOf(TextFieldValue()) }

    MainActivity.camResText.subscribe(
        { t ->
            textOriginal.value = t.originalText
            textEAnalysis.value = t.eAnalysis
            textTimestamp.value = t.timestamp
            isAllViewVisible = !t.empty()
        }
    )


    Box(modifier = modifier) {
        val lifecycleOwner = LocalLifecycleOwner.current
        val coroutineScope = rememberCoroutineScope()






        Box {
            AndroidView(
                modifier = Modifier.fillMaxSize().alpha(0.9999f), //alpha is fix for Camera tab. Without alpha not visible
                factory = { context ->
                    previewView = PreviewView(context).apply {
                        this.scaleType = scaleType
                        layoutParams = ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                        )
                    }
                    previewView
                }

            )

            CaptureButton(
                context = context,
                modifier = Modifier
                    .padding(30.dp)
                    .size(80.dp)
                    .align(Alignment.BottomCenter),
                onCaptureClick = {
                    coroutineScope.launch {
                        imageCaptureFinal.takePicture(context.executor).let {
                            if (!MainActivity.analyzeInProcess) {
                                MainActivity.analyzeInProcess = true
                                //vibraion on button click
                                VibrationUtils().vibrateOnButtonClick(context)
                                //threshold change
//                                val src: Mat = Imgcodecs.imread(it.absolutePath, Imgcodecs.IMREAD_GRAYSCALE)
//                                val dst: Mat =  Mat()
//                                Imgproc.adaptiveThreshold(src, dst, 255.0, opencv_imgproc.CV_ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY,41,0.0);
//                                val newTempName = it.absolutePath+"_thresh.jpg"
//                                Imgcodecs.imwrite(newTempName,dst)


                                //analyze picture
                                val harmAndNamesDto =
                                    PictureAnalyzer().analyze(it.absolutePath, context)
                                //vibration based on picture analysis
                                VibrationUtils().vibrateAfterAnalyzeToGiveFeedback(
                                    context = context,
                                    harmAndNamesDto = harmAndNamesDto
                                )
                                //vibraion on end
//                                VibrationUtils().vibrateOnEnd(context)
                                MainActivity.analyzeInProcess = false
                            }

                        }
                    }
                }
            )
            Box {
                Column(
                    modifier = Modifier
                        .padding(0.dp, 0.dp, 0.dp, 100.dp)
                        .alpha(if (isAllViewVisible) 0.7f else 0f),
                    verticalArrangement = Arrangement.Top,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Row {
                        Column(
                            modifier = Modifier
                                .fillMaxHeight(0.8f)
                                .verticalScroll(rememberScrollState())
                        ) {
                            Row {
                                TextField(
                                    modifier = Modifier
                                        .wrapContentSize(Alignment.Center)
                                        .fillMaxWidth(),
                                    value = textEAnalysis.value,
                                    colors = TextFieldDefaults.outlinedTextFieldColors(
                                        textColor = Color.White,
                                        backgroundColor = Color.Black,
                                    ),
                                    textStyle = MaterialTheme.typography.body1.copy(textAlign = TextAlign.Justify),
                                    readOnly = true,
                                    onValueChange = { textEAnalysis.value = it }
                                )
                            }

                            Row {
                                TextField(
                                    modifier = Modifier
                                        .wrapContentSize(Alignment.Center)
                                        .fillMaxWidth(),
                                    value = textOriginal.value,
                                    colors = TextFieldDefaults.outlinedTextFieldColors(
                                        textColor = Color.Black,
                                        backgroundColor = Color.White,
                                    ),
                                    textStyle = MaterialTheme.typography.body1.copy(textAlign = TextAlign.Justify),
                                    readOnly = true,
                                    onValueChange = { textOriginal.value = it },
                                )
                            }
                            Row {
                                TextField(
                                    modifier = Modifier
                                        .wrapContentSize(Alignment.Center)
                                        .fillMaxWidth(),
                                    value = textTimestamp.value,
                                    colors = TextFieldDefaults.outlinedTextFieldColors(
                                        textColor = Color.White,
                                        backgroundColor = Color.Transparent,
                                    ),
                                    textStyle = MaterialTheme.typography.body1.copy(textAlign = TextAlign.Justify),
                                    readOnly = true,
                                    onValueChange = { textTimestamp.value = it },
                                )
                            }
                        }
                    }
                    Row {
                        Button(
                            onClick = {
                                //clear text
                                MainActivity.camResText.onNext(
                                    CameraViewAnalysisDto(
                                        TextFieldValue(text = ""),
                                        TextFieldValue(text = ""),
                                        TextFieldValue(text = "")
                                    )
                                )
                            },
                            enabled = true,
                            modifier = Modifier.fillMaxWidth(),
                            shape = RoundedCornerShape(20.dp)
                        ) {
                            Text(text = LocalContext.current.getString(R.string.cameraparts_capture_clear_screen))
                        }
                    }
                }
            }
        }

        val cameraProviderFuture = ProcessCameraProvider.getInstance(context)
        cameraProviderFuture.addListener({
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
            // Create an extensions manager
            val extensionsManager =
                ExtensionsManager.getInstanceAsync(context, cameraProvider).get()

            val previewSmallBuilder = Preview.Builder()
            val camera2InterOpB = Camera2Interop.Extender(previewSmallBuilder)
            camera2InterOpB.setCaptureRequestOption(
                CaptureRequest.CONTROL_EFFECT_MODE,
                CONTROL_EFFECT_MODE_MONO
            )
            //not sure it is working
//            camera2InterOpB.setCaptureRequestOption(
//                CaptureRequest.CONTROL_SCENE_MODE,
//                CONTROL_SCENE_MODE_HDR
//            )

            val previewSmallFinal =
                previewSmallBuilder.setTargetRotation(previewView.display.rotation).build()

            val imageCapturebuilder = ImageCapture.Builder()
            val camera2InterOp = Camera2Interop.Extender(imageCapturebuilder)
            camera2InterOp.setCaptureRequestOption(
                CaptureRequest.CONTROL_EFFECT_MODE,
                CONTROL_EFFECT_MODE_MONO
            )
            //not sure it is working
//            camera2InterOpB.setCaptureRequestOption(
//                CaptureRequest.CONTROL_SCENE_MODE,
//                CONTROL_SCENE_MODE_HDR
//            )


            imageCaptureFinal = imageCapturebuilder
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY)
                .setFlashMode(FLASH_MODE_AUTO)
                .setFlashType(FLASH_TYPE_USE_TORCH_AS_FLASH)
                .setTargetRotation(previewView.display.rotation)
//                .setTargetAspectRatio(RATIO_4_3)
//                    .setMaxResolution(previewUseCase.resolutionInfo!!.resolution)
//            .setTargetResolution(Size(MainActivity.settingsStored.imageRecognitionResolution, MainActivity.settingsStored.imageRecognitionResolution))
                .build()


            val activity = context.findActivity()
            val windowMetrics: WindowMetrics =
                WindowMetricsCalculator.getOrCreate().computeCurrentWindowMetrics(activity);
            val height: Int = windowMetrics.bounds.height()
            val width: Int = windowMetrics.bounds.width()

            val viewPort = ViewPort.Builder(
                Rational(width, height),
                previewView.display.rotation
            ).build()
            previewSmallFinal.setSurfaceProvider(previewView.surfaceProvider)
            val useCaseGroup = UseCaseGroup.Builder()
                .addUseCase(previewSmallFinal)
                .addUseCase(imageCaptureFinal)
                .setViewPort(viewPort)
                .build()

            try {
                cameraProvider.unbindAll()
            } catch (ex: java.lang.Exception) {
                //nothing.
                ex.printStackTrace();
            }

            // Query if extension is available.
            if (extensionsManager.isExtensionAvailable(
                    cameraSelector,
                    ExtensionMode.HDR
                )
            ) {

                // Retrieve extension enabled camera selector
                val bokehCameraSelector = extensionsManager.getExtensionEnabledCameraSelector(
                    cameraSelector,
                    ExtensionMode.HDR
                )

                cameraProvider.bindToLifecycle(
                    lifecycleOwner,
                    bokehCameraSelector,
                    useCaseGroup
                )
            } else {
                cameraProvider.bindToLifecycle(
                    lifecycleOwner, cameraSelector, useCaseGroup
                )
            }
        }, ContextCompat.getMainExecutor(context))
    }
}

suspend fun Context.getCameraProvider(): ProcessCameraProvider = suspendCoroutine { continuation ->
    ProcessCameraProvider.getInstance(this).also { cameraProvider ->
        cameraProvider.addListener({
            continuation.resume(cameraProvider.get())
        }, ContextCompat.getMainExecutor(this))
    }
}

val Context.executor: Executor
    get() = ContextCompat.getMainExecutor(this)

//@SuppressLint("UnsafeOptInUsageError")
//suspend fun ImageAnalysis.analyzeImage(executor: Executor): String {
//    return withContext(Dispatchers.IO) {
//        setAnalyzer(executor) { imageProxy ->
//            imageProxy.image?.let {
//                //Log.d("ImageAnalysis", it.timestamp.toString())
//            }
//            imageProxy.close()
//        }
//
//        ""
//    }
//
//}

suspend fun ImageCapture.takePicture(executor: Executor): File {
    val photoFile = withContext(Dispatchers.IO) {
        kotlin.runCatching {
            File.createTempFile("image", "jpg")
        }.getOrElse { ex ->
//            Log.e("Capture", "Failed to create temporary file", ex)
            File("/dev/null")
        }
    }

    return suspendCoroutine { continuation ->
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()
        takePicture(
            outputOptions, executor,
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    continuation.resume(photoFile)
                }

                override fun onError(ex: ImageCaptureException) {
//                    Log.e("Capture", "Image capture failed", ex)
                    continuation.resumeWithException(ex)
                }
            }
        )
    }
}

fun Context.findActivity(): Activity {
    var context = this
    while (context is ContextWrapper) {
        if (context is Activity) return context
        context = context.baseContext
    }
    throw IllegalStateException("no activity")
}