package com.cleverdata.shopuicompose.ui.modesParts

import androidx.compose.foundation.layout.*
import androidx.compose.material.RadioButton
import androidx.compose.material.RadioButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.cleverdata.shopuicompose.MainActivity
import com.cleverdata.shopuicompose.R
import com.cleverdata.shopuicompose.commons.e.dto.SettingsDto
import com.cleverdata.shopuicompose.ui.dto.ModeType
import io.reactivex.subjects.BehaviorSubject


@Composable
fun VibrationSettingsComponent() {
    Row {
        Column {
            val selectedMode =
                remember { mutableStateOf(MainActivity.settingsStored.modeType) }
            Row {
                Text(
                    LocalContext.current.getString(R.string.vibrationsettings_modes_header),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                )
            }
            Row {
                Spacer(modifier = Modifier.size(16.dp))
                RadioButton(selected = selectedMode.value == ModeType.OneEBad.name, onClick = {
                    selectedMode.value = ModeType.OneEBad.name
                        MainActivity.settings.onNext(
                            SettingsDto(
                                MainActivity.settingsStored.timestamp,
                                MainActivity.settingsStored.edetails,
                                MainActivity.settingsStored.originalText,
                                selectedMode.value,
                                MainActivity.settingsStored.language,
                                MainActivity.settingsStored.impovedRecognition
                            )
                        )
                }, colors = RadioButtonDefaults.colors(Color.Blue))
                Text(LocalContext.current.getString(ModeType.OneEBad.resCode), modifier = Modifier.padding(0.dp, 12.dp))
            }

            Row {
                Spacer(modifier = Modifier.size(16.dp))
                RadioButton(selected = selectedMode.value == ModeType.GenerallyBad.name, onClick = {
                    selectedMode.value = ModeType.GenerallyBad.name
                    MainActivity.settings.onNext(
                        SettingsDto(
                            MainActivity.settingsStored.timestamp,
                            MainActivity.settingsStored.edetails,
                            MainActivity.settingsStored.originalText,
                            selectedMode.value,
                            MainActivity.settingsStored.language,
                            MainActivity.settingsStored.impovedRecognition
                        )
                    )
                }, colors = RadioButtonDefaults.colors(Color.Blue))
                Text(LocalContext.current.getString(ModeType.GenerallyBad.resCode), modifier = Modifier.padding(0.dp, 12.dp))
            }


            Row {
                Spacer(modifier = Modifier.size(16.dp))
                RadioButton(selected = selectedMode.value == ModeType.SilentVibrateMode.name, onClick = {
                    selectedMode.value = ModeType.SilentVibrateMode.name
                    MainActivity.settings.onNext(
                        SettingsDto(
                            MainActivity.settingsStored.timestamp,
                            MainActivity.settingsStored.edetails,
                            MainActivity.settingsStored.originalText,
                            selectedMode.value,
                            MainActivity.settingsStored.language,
                            MainActivity.settingsStored.impovedRecognition
                        )
                    )
                }, colors = RadioButtonDefaults.colors(Color.Blue))
                Text(LocalContext.current.getString(ModeType.SilentVibrateMode.resCode), modifier = Modifier.padding(0.dp, 12.dp))
            }

        }

    }
}
