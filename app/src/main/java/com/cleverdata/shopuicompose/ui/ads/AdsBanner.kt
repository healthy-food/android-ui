package com.cleverdata.shopuicompose.ui.ads

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds

class AdsBanner {


    @Composable
    fun AdvertView(modifier: Modifier = Modifier) {
        AndroidView(
            modifier = Modifier.fillMaxWidth(),
            factory = { context ->
                MobileAds.initialize(context) {}
                AdView(context).apply {
                    setAdSize(AdSize.BANNER)
                    modifier.fillMaxWidth()
                    adUnitId = "ca-app-pub-3957634285644556/5152539502"
//                    adUnitId = "ca-app-pub-3940256099942544/6300978111"
                    loadAd(AdRequest.Builder().build())
                }
            }
//            update = { adView ->
//                adView.context

//            }
        )
    }
}