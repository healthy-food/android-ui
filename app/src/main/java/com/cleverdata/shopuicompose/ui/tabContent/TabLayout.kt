package com.cleverdata.shopuicompose.ui.tabContent

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.cleverdata.shopuicompose.R
import com.cleverdata.shopuicompose.ui.ads.AdsBanner
import com.cleverdata.shopuicompose.ui.cameraParts.CaptureCameraTab
import com.google.accompanist.pager.*


@ExperimentalPagerApi // 1.
@Preview
@Composable
fun tabsWithSwiping() {
    var tabIndex by remember { mutableStateOf(0) }
    val tabTitles = listOf(LocalContext.current.getString(R.string.tabs_camera), LocalContext.current.getString(R.string.tabs_general), LocalContext.current.getString(R.string.tabs_modes))
    Column {
        Row{
            AdsBanner().AdvertView(Modifier.fillMaxWidth())
        }
        TabRow(selectedTabIndex = tabIndex) {
            tabTitles.forEachIndexed { index, title ->
                Tab(selected = tabIndex == index,
                    onClick = { tabIndex = index },
                    text = { Text(text = title) })
            }
        }
        when (tabIndex) {
            0 -> CaptureCameraTab()
            1 -> SettingsScreenForTab()
            2 -> ModesScreenForTab()
        }
    }
}




