package com.cleverdata.shopuicompose.ui.dto;

import com.cleverdata.shopuicompose.R;

public class ModeType {
//    public static String OneEBad = "Additive with danger 4 or 5 ";
//    public static String GenerallyBad = "4 and more additives found";
//    public static String NoVibration = "No vibration";
//    public static String SilentVibrateMode = "No vibration at all";
    public static OneType OneEBad = new OneType("OneEBad",R.string.vibrationsettings_modes_onebad);
    public static OneType GenerallyBad = new OneType("GenerallyBad", R.string.vibrationsettings_modes_generallybad);
    public static OneType SilentVibrateMode = new OneType("SilentVibrateMode",R.string.vibrationsettings_modes_silentvibratemode);


    public static class OneType{
        public String name;
        public int resCode;

        public OneType(String name, int resCode) {
            this.name = name;
            this.resCode = resCode;
        }
    }
}
