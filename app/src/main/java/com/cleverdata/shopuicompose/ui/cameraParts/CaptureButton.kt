package com.cleverdata.shopuicompose.ui.cameraParts

import android.content.Context
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun CaptureButton(
    modifier: Modifier = Modifier,
    context: Context,
    onCaptureClick: () -> Unit = { },
) {
    //button is enabled by default
    var buttonDisabled = remember { mutableStateOf(false) }

    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()
    val color = if (isPressed) Color.Green else Color.Blue
//    val contentPadding = PaddingValues(if (isPressed) 6.dp else 20.dp)
    val contentPadding = PaddingValues(if (isPressed) 40.dp else 2.dp)


    Button(
        modifier = modifier.fillMaxSize(),
        border = BorderStroke(5.dp, Color.White),
        contentPadding = contentPadding,
        shape = CircleShape,
        colors = ButtonDefaults.buttonColors(
            backgroundColor = color,
            disabledBackgroundColor = Color.Red,
            disabledContentColor = Color.Yellow
        ),
        interactionSource = interactionSource,
//        enabled = ,
        onClick = onCaptureClick,
        enabled = true
    ) {
        // No content
    }


}